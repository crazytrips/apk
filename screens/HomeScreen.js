import React from "react";
import {View, Text, SafeAreaView, StyleSheet, ScrollView} from 'react-native';
import Header from "../components/home/Header";
import Stories from "../components/home/Stories";
import Post from "../components/home/Post";
import {POSTS} from "../data/posts";
import BottomTabs from "../components/home/BottomTabs";
import {BOTTOM_TABS_ICONS} from "../data/bottomTabsIcons";

const HomeScreen = () => {
    return (
        <SafeAreaView style={styles.container}>
            <Header />
            <Stories />
            <ScrollView>
                {POSTS.map((post, index) => (
                    <Post post={post} key={index} />
                ))}
            </ScrollView>
            <BottomTabs icons={BOTTOM_TABS_ICONS}/>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
    },
});

export default HomeScreen;