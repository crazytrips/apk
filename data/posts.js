import {USERS} from "./users";

export const POSTS = [
    {
        imageUrl: 'https://i.ibb.co/182bP1y/4k.png',
        user: USERS[0].user,
        likes: 7870,
        caption: 'Train Ride to Hogwarts. ',
        profile_picture: USERS[0].image,
        comments: [
            {
                user: 'theqazman',
                comment: 'Ale bajera'
            },
            {
                user: 'theqazman',
                comment: 'Superowo, gdzie to jest ?'
            },
        ]
    },
    {
        imageUrl: 'https://i.ibb.co/182bP1y/4k.png',
        user: USERS[0].user,
        likes: 7870,
        caption: 'Train Ride to Hogwarts. ',
        profile_picture: USERS[0].image,
        comments: [
            {
                user: 'theqazman',
                comment: 'Ale bajera'
            },
        ]
    }
];

export const POST_FOOTER_ICONS = [
    {
        name: 'Like',
        imageUrl: 'https://img.icons8.com/fluency-systems-regular/60/ffffff/like--v1.png',
        likedImageUrl: 'https://img.icons8.com/ios-glyphs/30/ffffff/like--v1.png'
    },
    {
        name: 'Comment',
        imageUrl: 'https://img.icons8.com/material-outlined/24/ffffff/speech-bubble--v1.png'
    },
    {
        name: 'Share',
        imageUrl: 'https://img.icons8.com/ios-glyphs/30/ffffff/paper-plane.png'
    },
    {
        name: 'Save',
        imageUrl: 'https://img.icons8.com/material-outlined/24/ffffff/bookmark-ribbon--v1.png'
    }
];