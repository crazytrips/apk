import React from "react";
import {View, Text, StyleSheet, Image, ScrollView} from 'react-native';
import {USERS} from "../../data/users";


const Stories = () => {
    return (
        <View style={styles.container}>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                {USERS.map((story, index) => (
                    <View key={index} style={styles.imageBox}>
                        <Image
                            source={{ 'uri': story.image }}
                            style={styles.story}
                        />
                        <Text style={styles.text}>
                            {story.user.length > 11
                                ? story.user.slice(0, 10).toLowerCase() + '...'
                                : story.user.toLowerCase()}
                        </Text>
                    </View>
                ))}
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 13
    },
    text: {
      color: 'white'
    },
    story: {
        width: 70,
        height: 70,
        borderRadius: 50,
        marginLeft: 6,
        borderWidth: 3,
        borderColor: '#ff8501'
    },
    imageBox: {
        alignItems: 'center'
    }
});
export default Stories;