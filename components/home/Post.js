import React from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Divider} from 'react-native-elements';
import { POST_FOOTER_ICONS } from "../../data/posts";

const Post = ({ post }) => {
    return (
        <View style={styles.container}>
            <Divider width={1} orientation='vertical' />
            <PostHeader post={post} />
            <PostImage post={post} />
            <View style={styles.postFooterBox}>
                <PostFooter />
                <Likes post={post} />
                <Caption post={post} />
                <CommentsSection post={post} />
                <Comments post={post} />
            </View>
        </View>
    );
}

const PostHeader = ({ post }) => (
    <View style={styles.postHeaderContainer}>
        <View style={styles.postHeaderProfileBox}>
            <Image source={{
                uri: post.profile_picture
            }} style={styles.story}/>
            <Text style={styles.postHeaderTextProfile}>
                {post.user}
            </Text>
        </View>
        <Text style={styles.text}>...</Text>
    </View>
)

const PostImage = ({ post }) => (
    <View style={styles.postImageBox}>
        <Image
            source={{uri: post.imageUrl}}
            style={styles.postImage}
        />
    </View>
)

const PostFooter = () => (
    <View style={styles.postFooterIconsBox}>
        <View style={styles.leftFooterIconsBox}>
            <Icon imgStyle={styles.footerIcon} imgUrl={POST_FOOTER_ICONS[0].imageUrl}/>
            <Icon imgStyle={styles.footerIcon} imgUrl={POST_FOOTER_ICONS[1].imageUrl}/>
            <Icon imgStyle={[styles.footerIcon, styles.shareIcon]} imgUrl={POST_FOOTER_ICONS[2].imageUrl}/>
        </View>
        <View>
            <Icon imgStyle={styles.footerIcon} imgUrl={POST_FOOTER_ICONS[3].imageUrl}/>
        </View>
    </View>
)

const Icon = ({imgStyle, imgUrl}) => (
    <TouchableOpacity>
        <Image style={imgStyle} source={{uri: imgUrl}} />
    </TouchableOpacity>
)

const Likes = ({ post }) => (
    <View style={styles.likesBox}>
        <Text style={styles.text}>{post.likes.toLocaleString('en')} likes</Text>
    </View>
)

const Caption = ({ post }) => (
    <View style={styles.captionsBox} >
        <Text style={styles.captionsText}>
            <Text style={styles.captionAuthor}>{post.user}</Text>
            <Text> {post.caption}</Text>
        </Text>
    </View>
)

const CommentsSection = ({ post }) => (
    <View style={styles.commentsSectionBox}>
        {!!post.comments.length && (
            <Text style={styles.commentsSectionText}>
                View{post.comments.length > 1 ? ' all' : ''} {post.comments.length}
                { post.comments.length > 1 ? ' comments' : ' comment' }
            </Text>
        )}
    </View>
)

const Comments = ({ post }) => (
    <>
        {post.comments.map((comment, index) => (
            <View key={index} style={styles.commentsBox}>
                <Text style={styles.commentsText}>
                    <Text style={styles.commentsTextUser}>{comment.user}</Text>{' '}
                    {comment.comment}
                </Text>
            </View>
        ))}
    </>
)

const styles = StyleSheet.create({
    container: {
        marginBottom: 30,
    },
    text: {
        color: 'white',
        fontWeight: '900'
    },
    postHeaderContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 5,
        alignItems: "center"
    },
    story: {
        width: 35,
        height: 35,
        borderRadius: 50,
        marginLeft: 6,
        borderWidth: 1.6,
        borderColor: '#ff8501'
    },
    postHeaderTextProfile: {
        color: 'white',
        marginLeft: 5,
        fontWeight: '700'
    },
    postHeaderProfileBox: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    postImage: {
        height: '100%',
        resizeMode: 'cover'
    },
    postImageBox: {
        width: '100%',
        height: 450
    },
    footerIcon: {
        width: 33,
        height: 33
    },
    postFooterBox: {
        marginHorizontal: 15,
        marginTop: 10
    },
    postFooterIconsBox: {
        flexDirection: 'row'
    },
    leftFooterIconsBox: {
        flexDirection: 'row',
        width: '32%',
        justifyContent: 'space-between'
    },
    shareIcon: {
        transform: [{ rotate: '320deg' }],
        marginTop: -3
    },
    rightFooterIconsBox: {
        alignItems: 'flex-end',
        flex: 1
    },
    likesBox: {
        flexDirection: 'row',
        marginTop: 4
    },
    captionsBox: {
        marginTop: 5
    },
    captionsText: {
        color: 'white'
    },
    captionAuthor: {
        fontWeight: '700'
    },
    commentsSectionText: {
        color: 'gray'
    },
    commentsSectionBox: {
        marginTop: 5
    },
    commentsText: {
        color: 'white'
    },
    commentsTextUser: {
        fontWeight: 'bold'
    },
    commentsBox: {
        flexDirection: 'row',
        marginTop: 5
    }
});

export default Post;